const express = require('express');
const app = express();
const cors = require('cors');
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const { PORT = 3000 } = process.env;
const jwt = require('jsonwebtoken');
const fs = require('fs');

app.use(express.json());
app.use(cors());

								// app.get('/login', (res,req) => {
								// 	let privateKey = fs.readFileSync('./private.pem', 'utf8');
								// 	let token = jwt.sign({ "user": "user" }, privateKey, { algoritm: 'RS256'});
								// 	res.send(token);
								// 	console.log(token);
								// })

								// app.post('/verifytoken', (req,res) => {
								// 	let publicKey = fs.readFileSync('./public.pem', 'utf8');
								// 	let decoded = jwt.verify(req.body.token, publicKey, {complete:true});
								// 	res.send(decoded);
								// 	console.log(decoded);
								// })

app.get('/', (req, res) => {
	res.sendFile(__dirname + '../chat-app/src/App.js');
});

let users = [];
let clients = [];

io.on('connect', (socket) => {
	console.log('A user connected')
	socket.send({user:"SERVER", message:'You have connected to the chat!'});
	socket.broadcast.emit('message', {user: 'SERVER', message: "new user connected"});


	socket.on('message', (data) => {
		socket.emit('message', data);
		console.log(data + ": "+ data.message);

		socket.broadcast.emit('message', data)

	});

	socket.on('username', (data) => {
        console.log(data + ' username')
        if(users.indexOf(data) < 0){
            users.push(data)
            clients.push({username: data, socketId: socket.id}) 
            console.log(users)      
            socket.emit('addUsers', users)
            console.log(users);
            socket.broadcast.emit('addUser',  data)
        }
    });

	socket.on('disconnect', () => {
		socket.broadcast.emit('message', {user: "SERVER", message:  "User left ..."})
		let index
		let user
		//hitta rätt socket
		for( let i = 0; i < clients.length; i++ ) {
			if (clients[i].socketId == socket.id) {
				user = clients[i].username;
				clients.splice(i, 1);
			}
		}
		//ta bort usern på den rätta socketen
		for (let i = 0; i < users.length; i++) {
			if(user == users[i]) {
				console.log('removing ', user);
				socket.broadcast.emit('removeUser', user);
				index = i;
			}
		}
		users.splice(index,1)
		console.log('user disconnected');
	});
});


http.listen(PORT, () => {
	console.log('listening on *:' + PORT);
});

